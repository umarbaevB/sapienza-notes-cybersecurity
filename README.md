# Sapienza Notes - Cybersecurity

## Description

In this repo will be uploaded all the notes wrote by me during the period of my master's degree at Sapienza unviersity of Rome (Course: Cybersecurity)

## Disclaimer

This is a reorganization and redigitalization work more than a summary.

The stuff in this repo is based on professors' slides; there could be typing or concept errors in my summaries so keep an eye open.

## Important

- "Summaries" topics could not follow slides arguments.

## Contributors

- Irene Orchi
- Carmine Molisso

## Index

- [Distributed Systems](./sys_dist/README.md)
- [Data and Network Security](./data_sec/README.md)
- [Practical Network Defense](./net_def/README.md)
- [Ethical Hacking](./eth_hack/README.md)
- [Network Infrastructures](./net_inf/README.md)
- [Cyber and Computer Law (ITA)](./cyb_law/README.md)
- [Advanced Operating Systems and Virtualization](./aosv/README.md)
- [Malware Analysis](./malware_analysis/README.md)
- [Security Governance](./sec_gov/README.md)
- [Crypto](./crypto/README.md)
- [Statistics](./statistics/README.md)
- [Web Security and Privacy](./web_sec/README.md)
- [Risk Management](./risk_management/README.md)
- [Laboratory of Network Design and Configuration](./lab_net/README.md)
- [Cybersecurity Seminars](./seminars/README.md)
